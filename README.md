## PROJETO CHAT

*Equipe: Diones Gomes e Matheus Ramyres* 

## Criando banco de dados

criar banco postgresql com nome 'sd'

OBS: caso seu banco tenha senha e usuario diferentes do da aplicação, você pode altera-las nos respectivos arquivos "Knexfile.js" e "connection.js"

user=postgres
passwor=postgres

## Rodando o projeto

Na pasta raiz do projeto backend "SDChat" executar os comandos

`yarn install`
`yarn prestart`
`yarn dev`

Na raiz do projeto frontend na pasta "client" executar os comando:

`yarn install`
`yarn dev`
